import * as authActions from './auth';
import * as burgerActions from './burgerBuilder';
import * as orderActions from './orders';

const actionCreators = { ...authActions, ...burgerActions, ...orderActions };

export default actionCreators;