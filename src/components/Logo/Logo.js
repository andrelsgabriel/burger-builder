import React from 'react';
import classes from './Logo.module.css';
import logoPath from '../../assets/burger-logo.png';

const logo = () => (
    <div className={classes.Logo} >
        <img src={logoPath} alt='MyBurger' />
    </div>
)

export default logo;