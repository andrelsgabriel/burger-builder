import reducer from './auth';
import * as actionTypes from '../actions/actionTypes';

describe('Auth Reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            token: null,
            userId: null,
            error: null,
            loading: false,
            authRedirectPath: '/'
        });
    });

    it('should save token on success login', () => {
        const state = {
            token: null,
            userId: null,
            error: null,
            loading: false,
            authRedirectPath: '/'
        };

        const action = { type: actionTypes.AUTH_SUCCESS, token: 'token', userId: 'userId' };

        const result = {
            token: 'token',
            userId: 'userId',
            error: null,
            loading: false,
            authRedirectPath: '/'
        };

        expect(reducer(state, action)).toEqual(result);
    });
});