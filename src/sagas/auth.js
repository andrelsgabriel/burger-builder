import { put, delay } from 'redux-saga/effects';

import * as actions from '../store/actions/auth';
import axios from '../axios/axios-auth';

export function* logout(action) {
    yield localStorage.removeItem('token');
    yield localStorage.removeItem('userId');
    yield localStorage.removeItem('expirationDate');
    yield put(actions.logoutSucceed());
}

export function* checkAuthTimeout(action) {
    yield delay(action.expirationTime * 1000);
    yield put(actions.initiateLogout());
}

export function* saveTokeninLocalStorage(action) {
    const expirationDate = new Date(new Date().getTime() + action.expiresIn * 1000);
    yield localStorage.setItem('token', action.token);
    yield localStorage.setItem('userId', action.userId);
    yield localStorage.setItem('expirationDate', expirationDate);
}

export function* authUser(action) {
    yield put(actions.authStart());
    const authData = {
        email: action.email,
        password: action.password,
        returnSecureToken: true
    }
    let url = 'verifyPassword'
    if (action.isSignup) {
        url = 'signupNewUser';
    }

    try {
        const res = yield axios.post(url, authData);
        yield put(actions.saveTokeninLocalStorage(res.data.idToken, res.data.localId, res.data.expiresIn));
        yield put(actions.authSuccess(res.data.idToken, res.data.localId));
        yield put(actions.checkAuthTimeout(res.data.expiresIn));
    } catch (err) {
        yield put(actions.authFail(err.response.data.error));
    }
}

export function* authCheckData(action) {
    const token = yield localStorage.getItem('token');
    const userId = yield localStorage.getItem('userId');
    const expirationDate = yield new Date(localStorage.getItem('expirationDate'));

    if (token && (expirationDate > new Date())) {
        yield put(actions.authSuccess(token, userId));
        const expirationSeconds = (expirationDate.getTime() - new Date().getTime()) / 1000;
        yield put(actions.checkAuthTimeout(expirationSeconds));
    }
}