import React from 'react';
import classes from './Order.module.css';

const order = (props) => {

    const ingredients = [];

    for (let inName in props.ingredients) {
        ingredients.push({
            name: inName,
            amount: props.ingredients[inName]
        });
    }

    const ingredientsOutput = ingredients.map(ig => (
        <span
            style={{
                textTransform: 'capitalize',
                display: 'inline-block',
                margin: '2px 8px',
                border: '1px solid #ccc',
                padding: '5px'
            }}
            key={ig.name}>{ig.name} ({ig.amount})
        </span>
    ));

    return (
        <div className={classes.Order}>
            <p>Ingredient: {ingredientsOutput}</p>
            <p>Total Price: <strong>NZD {props.price.toFixed(2)}</strong></p>
        </div>
    );

}

export default order;