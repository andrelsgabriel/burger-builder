import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import classes from './Auth.module.css';
import * as actionCreators from '../../store/actions/auth';
import Spinner from '../../components/UI/Spinner/Spinner';
import { updateObject, checkValidity } from '../../shared/utility';

const createProperty = (placeholder, type = 'text') => {

    const validation = { required: true };

    if (type === 'email') {
        validation.isEmail = true;
    } else {
        validation.minLength = 7;
    }

    return ({
        elementType: 'input',
        elementConfig: {
            type: type,
            placeholder: placeholder
        },
        value: '',
        valid: false,
        touched: false,
        validation: validation
    });
}

class Auth extends Component {

    state = {
        controls: {
            email: createProperty('Mail Address', 'email'),
            password: createProperty('Password', 'password'),
        },
        formIsValid: false,
        isSignup: true
    }

    componentDidMount() {
        if (!this.props.buildingBurger && this.props.authRedirectPath !== '/') {
            this.props.onSetAuthRedirectPath();
        }
    }

    inputChangedHandler = (event, controlName) => {

        const updatedControl = updateObject(this.state.controls[controlName], {
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
            touched: true
        });

        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updatedControl
        });

        let formIsValid = true;
        for (let el in updatedControls) {
            formIsValid = updatedControls[el].valid && formIsValid;
        }
        this.setState({ controls: updatedControls, formIsValid: formIsValid });
    }

    submitHandler = (event) => {
        event.preventDefault();
        this.props.onAuth(this.state.controls.email.value, this.state.controls.password.value, this.state.isSignup);
    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return { isSignup: !prevState.isSignup };
        })
    }

    render() {
        const formElArray = [];
        for (let key in this.state.controls) {
            formElArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElArray.map(formEl => (
            <Input
                elementType={formEl.config.elementType}
                elementConfig={formEl.config.elementConfig}
                value={formEl.config.value}
                invalid={!formEl.config.valid}
                shouldValidate={formEl.config.validation}
                touched={formEl.config.touched}
                changed={(event) => this.inputChangedHandler(event, formEl.id)}
                key={formEl.id} />
        ));

        if (this.props.loading) {
            form = <Spinner />;
        }

        let errorMsg = null;
        if (this.props.error) {
            errorMsg = (
                <p style={{ color: 'red' }}>{this.props.error.message}</p>
            );
        }

        let authRedirect = null;
        if (this.props.isAuthenticate) {
            authRedirect = <Redirect to={this.props.authRedirectPath} push={true} />;
        }

        return (
            <div className={classes.Auth}>
                {authRedirect}
                <form onSubmit={this.submitHandler}>
                    <h3>{this.state.isSignup ? 'SIGNUP' : 'SIGNIN'}</h3>
                    {errorMsg}
                    {form}
                    <Button btnType='Success' disabled={!this.state.formIsValid}>
                        SUBMIT
                    </Button>
                </form>
                <Button
                    clicked={this.switchAuthModeHandler}
                    btnType='Danger'>
                    SWITCH TO {this.state.isSignup ? 'SIGNIN' : 'SIGNUP'}
                </Button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticate: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath,
        buildingBurger: state.burgerBuilder.building
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password, isSignup) => dispatch(actionCreators.auth(email, password, isSignup)),
        onSetAuthRedirectPath: () => dispatch(actionCreators.setAuthRedirectPath('/'))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);