import { takeEvery, takeLatest, all } from 'redux-saga/effects';

import * as authSagas from './auth';
import * as burgerSagas from './burgerBuilder';
import * as ordersSagas from './orders';
import * as actionTypes from '../store/actions/actionTypes';

export function* watchActionsEffects() {

    yield all([
        takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, authSagas.logout),
        takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, authSagas.checkAuthTimeout),
        takeEvery(actionTypes.AUTH_SAVE_TOKEN, authSagas.saveTokeninLocalStorage),
        takeEvery(actionTypes.AUTH_USER, authSagas.authUser),
        takeEvery(actionTypes.AUTH_CHECK_DATA, authSagas.authCheckData)
    ]);

    yield takeEvery(actionTypes.FETCH_INGREDIENTS, burgerSagas.fetchIngredients);

    yield takeLatest(actionTypes.PURCHASE_BURGER, ordersSagas.purchaseBurger);
    yield takeEvery(actionTypes.FETCH_ORDERS, ordersSagas.fetchOrders);
}