import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/',
    params: { key: 'AIzaSyCR8SeTKyGxrcUoKQrikCSslKbXvLeeh8Q' }
});

export default instance;