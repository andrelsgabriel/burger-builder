import { put } from 'redux-saga/effects';

import axios from '../axios/axios-orders';
import * as actions from '../store/actions/burgerBuilder';

export function* fetchIngredients(action) {

    try {
        const response = yield axios.get('ingredients.json');
        yield put(actions.setIngredient(response.data));
    } catch (error) {
        yield put(action.fetchIngredientsFailed());
    }
}