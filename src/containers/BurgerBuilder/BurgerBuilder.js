import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from '../../axios/axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import actionCreators from '../../store/actions';

export class BurgerBuilder extends Component {

    state = {
        purchasing: false
    }

    componentDidMount() {
        this.props.onInitIngredients();
    }

    isPurchasable = () => {

        const sum = Object.keys(this.props.ingredients)
            .map(key => (
                this.props.ingredients[key]
            ))
            .reduce((sum, el) => (
                sum += el
            ), 0);
        return sum > 0;
    }

    purchaseHandler = () => {
        if (this.props.isAuthenticate) {
            this.setState((prevState) => (
                { purchasing: !prevState.purchasing }
            ));
        } else {
            this.props.onSetRedirectPath('/checkout');
            this.props.history.push('/auth');
        }
    }

    purchaseContinueHandler = () => {
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    render() {
        const disabedInfo = {
            ...this.props.ingredients
        }
        for (let key in disabedInfo) {
            disabedInfo[key] = disabedInfo[key] <= 0;
        }

        let orderSummary = null;
        let burger = this.props.error ? <p style={{ textAlign: 'center' }}>Ingredients can't be loaded.</p> : <Spinner />;

        if (this.props.ingredients) {
            burger = (
                <Fragment>
                    <Burger ingredients={this.props.ingredients} />
                    <BuildControls
                        ingredientAdded={this.props.onIngredientAdded}
                        ingredientRemoved={this.props.onIngredientRemoved}
                        disabled={disabedInfo}
                        purchasable={this.isPurchasable()}
                        ordered={this.purchaseHandler}
                        isAuth={this.props.isAuthenticate}
                        price={this.props.price} />
                </Fragment>
            );

            orderSummary = (
                <OrderSummary
                    purchaseCancelled={this.purchaseHandler}
                    purchaseContinued={this.purchaseContinueHandler}
                    price={this.props.price}
                    ingredients={this.props.ingredients} />
            );
        }

        return (
            <Fragment>
                <Modal show={this.state.purchasing} clicked={this.purchaseHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.price,
        error: state.burgerBuilder.error,
        isAuthenticate: state.auth.token !== null
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: (ingName) => dispatch(actionCreators.addIngredient(ingName)),
        onIngredientRemoved: (ingName) => dispatch(actionCreators.removeIngredient(ingName)),
        onInitIngredients: () => dispatch(actionCreators.initIngredients()),
        onInitPurchase: () => dispatch(actionCreators.purchaseInit()),
        onSetRedirectPath: (path) => dispatch(actionCreators.setAuthRedirectPath(path))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));