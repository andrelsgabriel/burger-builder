import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';

import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class Layout extends Component {

    state = {
        displaySidedrawer: false
    };

    sideDrawerHandler = () => {
        this.setState((prevState) => ({
            displaySidedrawer: !prevState.displaySidedrawer
        }));
    }

    render() {
        return (
            <Fragment>
                <Toolbar
                    toggleClicked={this.sideDrawerHandler}
                    isAuthenticated={this.props.isAuthenticated} />
                <SideDrawer
                    open={this.state.displaySidedrawer}
                    closed={this.sideDrawerHandler}
                    isAuthenticated={this.props.isAuthenticated} />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    }
}

export default connect(mapStateToProps)(Layout);