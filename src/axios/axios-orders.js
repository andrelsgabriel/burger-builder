import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://myburgerreact.firebaseio.com/'
});

export default instance;