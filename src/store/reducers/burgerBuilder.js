import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    ingredients: null,
    price: 4,
    error: false,
    building: false
}

const INGREDIENTS_PRICE = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
};

const handleIngredient = (state, action) => {
    const ingQnt = action.type === actionTypes.ADD_INGREDIENT ? 1 : -1;
    const ingPrice = action.type === actionTypes.ADD_INGREDIENT ? INGREDIENTS_PRICE[action.ingredientName] : -INGREDIENTS_PRICE[action.ingredientName];
    const updatedIng = { [action.ingredientName]: state.ingredients[action.ingredientName] + ingQnt };
    const updatedIngs = updateObject(state.ingredients, updatedIng);
    const updatedState = {
        ingredients: updatedIngs,
        price: state.price + ingPrice,
        building: true
    };
    return updateObject(state, updatedState);
}

const setIngredients = (state, action) => {
    const updatedState = {
        ingredients: action.ingredients,
        price: 4,
        error: false,
        building: false
    };
    return updateObject(state, updatedState);

}

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case (actionTypes.ADD_INGREDIENT): return handleIngredient(state, action);
        case (actionTypes.REMOVE_INGREDIENT): return handleIngredient(state, action);
        case (actionTypes.SET_INGREDIENTS): return setIngredients(state, action);
        case (actionTypes.FETCH_INGREDIENTS_FAILED): return updateObject(state, { error: true });
        default: return state;
    }
}

export default reducer;