import React, {Fragment, memo} from 'react';
import classes from './Modal.module.css';
import Backdrop from '../../UI/Backdrop/Backdrop';

const modal = (props) => (
    <Fragment>
        <Backdrop show={props.show} clicked={props.clicked} />
        <div
            className={classes.Modal}
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}>
            {props.children}
        </div>
    </Fragment>
);

export default memo(modal);