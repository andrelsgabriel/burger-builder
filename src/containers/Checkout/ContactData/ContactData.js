import React, { Component } from 'react';
import { connect } from 'react-redux';

import classes from './ContactData.module.css';
import Button from '../../../components/UI/Button/Button';
import axios from '../../../axios/axios-orders';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import * as actionCreators from '../../../store/actions/orders';
import { updateObject, checkValidity } from '../../../shared/utility';

const createProperty = (placeholder, type = 'text', elementType = 'input') => {

    let elementConfig = ({
        type: type,
        placeholder: placeholder
    });

    if (elementType === 'select') {
        elementConfig = ({
            options: [
                { value: 'fastest', displayValue: 'Fastest' },
                { value: 'cheapest', displayValue: 'Cheapest' }
            ]
        });
    }

    const validation = {};
    if (elementType !== 'select') {
        validation.required = true;
    }
    if (elementType === 'email') {
        validation.isEmail = true;
    }
    if (placeholder === 'ZIP Code') {
        validation.minLength = 4;
        validation.maxLength = 4;
        validation.isNumeric = true;
    }

    return ({
        elementType: elementType,
        elementConfig: elementConfig,
        value: elementType === 'select' ? 'fastest' : '',
        valid: elementType === 'select' ? true : false,
        touched: elementType === 'select' ? true : false,
        validation: validation
    });
}

class ContactData extends Component {

    state = {
        orderForm: {
            name: createProperty('Your Name'),
            street: createProperty('Street'),
            zipCode: createProperty('ZIP Code'),
            country: createProperty('Country'),
            email: createProperty('Your E-Mail', 'email'),
            deliveryMode: createProperty(null, null, 'select')
        },
        formIsValid: false
    }

    orderHandler = (event) => {
        event.preventDefault();

        const formData = {};
        for (let elId in this.state.orderForm) {
            formData[elId] = this.state.orderForm[elId].value;
        }
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            userId: this.props.userId,
            orderData: formData
        };
        this.props.onOrderBurger(order, this.props.token);
    }

    inputChangedHandler = (event, formId) => {

        const updatedEl = updateObject(this.state.orderForm[formId], {
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.orderForm[formId].validation),
            touched: true
        });
        const updatedForm = updateObject(this.state.orderForm, {
            [formId]: updatedEl
        });

        let formIsValid = true;
        for (let el in updatedForm) {
            formIsValid = updatedForm[el].valid && formIsValid;
        }
        this.setState({ orderForm: updatedForm, formIsValid: formIsValid });
    }

    render() {
        const formElArray = [];
        for (let key in this.state.orderForm) {
            formElArray.push({
                id: key,
                config: this.state.orderForm[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElArray.map(formEl => (
                    <Input
                        elementType={formEl.config.elementType}
                        elementConfig={formEl.config.elementConfig}
                        value={formEl.config.value}
                        invalid={!formEl.config.valid}
                        shouldValidate={formEl.config.validation}
                        touched={formEl.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formEl.id)}
                        key={formEl.id} />
                ))}
                <Button btnType='Success' disabled={!this.state.formIsValid}>ORDER</Button>
            </form>
        );
        if (this.props.loading) {
            form = <Spinner />;
        }
        return (
            <div className={classes.ContactData} >
                <h4>Enter your contact data</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.price,
        loading: state.orders.loading,
        token: state.auth.token,
        userId: state.auth.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onOrderBurger: (order, token) => dispatch(actionCreators.purchaseBurger(order, token))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axios));