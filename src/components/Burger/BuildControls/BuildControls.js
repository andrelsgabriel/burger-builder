import React from 'react';
import classes from './BuildControls.module.css'
import BuildControl from './BuildControl/BuildControl';

const controls = [
    { label: 'Salad', value: 'salad' },
    { label: 'Bacon', value: 'bacon' },
    { label: 'Cheese', value: 'cheese' },
    { label: 'Meat', value: 'meat' }
];

const buildControls = (props) => (

    <div className={classes.BuildControls}>
        <p>Current price: <strong>{props.price.toFixed(2)}</strong></p>
        {controls.map(ctrl => (
            <BuildControl
                key={ctrl.label}
                label={ctrl.label}
                added={() => props.ingredientAdded(ctrl.value)}
                removed={() => props.ingredientRemoved(ctrl.value)}
                disabled={props.disabled[ctrl.value]} />
        ))}
        <button className={classes.OrderButton}
            onClick={props.ordered}
            disabled={!props.purchasable}>
            {props.isAuth ? 'ORDER NOW' : 'SIGNUP TO ORDER'} </button>
    </div>
);

export default buildControls;