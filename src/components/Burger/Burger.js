import React from 'react';
import classes from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const ingredientsWheigth = {
    salad: 1,
    bacon: 2,
    cheese: 3,
    meat: 4
}

const burger = (props) => {

    let transformedIngridients = Object.keys(props.ingredients)
        .sort((a, b) => (ingredientsWheigth[a] - ingredientsWheigth[b]))
        .map(key => {
            return [...Array(props.ingredients[key])].map((_, index) =>
                <BurgerIngredient key={key + index} type={key} />
            );
        })
        .reduce((arr, el) => {
            return arr.concat(el);
        }, []);

    if (transformedIngridients.length === 0) {
        transformedIngridients = <p>Please start adding ingredients!</p>;
    }
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {transformedIngridients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
}

export default burger;